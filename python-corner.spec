%global srcname corner

Name:		python-corner
Version:	2.0.1
Release:	1.2%{?dist}
Summary:	Python package to generate corner plots

License:	BSD
URL:		https://corner.readthedocs.io/
Source0:	https://files.pythonhosted.org/packages/65/af/a7ba022f2d5787f51db91b5550cbe8e8c40a6eebd8f15119e743a09a9c19/corner-2.0.1.tar.gz

Requires:	python%{python3_pkgversion}-matplotlib
Requires:	python%{python3_pkgversion}-dateutil
Requires:	python%{python3_pkgversion}-nose
Requires:	python%{python3_pkgversion}-pandas

BuildRequires:	python%{python3_pkgversion}-devel
BuildRequires:	python%{python3_pkgversion}-setuptools
BuildRequires:	python%{python3_pkgversion}-matplotlib
BuildRequires:	python%{python3_pkgversion}-dateutil
BuildRequires:	python%{python3_pkgversion}-nose
BuildRequires:	python%{python3_pkgversion}-pandas

%description
This Python module uses matplotlib to visualize multidimensional samples using a scatterplot matrix. In these visualizations, each one- and two-dimensional projection of the sample is plotted to reveal covariances. corner was originally conceived to display the results of Markov Chain Monte Carlo simulations and the defaults are chosen with this application in mind but it can be used for displaying many qualitatively different samples.

%package -n python%{python3_pkgversion}-%{srcname}
Summary:	%{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname}
This Python module uses matplotlib to visualize multidimensional samples using a scatterplot matrix. In these visualizations, each one- and two-dimensional projection of the sample is plotted to reveal covariances. corner was originally conceived to display the results of Markov Chain Monte Carlo simulations and the defaults are chosen with this application in mind but it can be used for displaying many qualitatively different samples.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build


%install
%py3_install

%check
%{__python3} setup.py test


%files -n python%{python3_pkgversion}-%{srcname}
%doc
%{python3_sitelib}/%{srcname}
%{python3_sitelib}/%{srcname}*.egg-info/



%changelog
* Fri Oct 4 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 2.0.1-1.2
- Drop python3.4 support

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 2.0.1-1.1
- Add python3.6 support

* Fri Nov 2 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 2.0.1-1
- Initial package
